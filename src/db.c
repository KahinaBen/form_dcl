#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "elmlist.h"
#include "list.h"
#include "db.h"

void write_list(struct list * L, enum mode_t mode, void (*ptrf)()) {
  FILE * fd;

  do {
    char full_file_name[40], file_name[20];
    printf("\t\tWrite list into TEXT/BIN file located in './data' directory\n");
    printf("\tFile name :");
    scanf("%s", file_name);
    sprintf(full_file_name, "./data/%s", file_name);
    fd = fopen(full_file_name, (mode == TEXT) ? "wt" : "wb");
  } while(!fd);

  for(struct elmlist * iter = get_head(L); iter; iter = get_suc(iter) ) {
    (*ptrf)(get_data(iter), mode, fd);
  }

  fclose(fd);
}

struct list * read_list(
  enum mode_t mode,
  void * (*read_data_ptrf)(),
  bool (*cmp_data_ptrf)()
) {
  FILE * fd;

  do {
    char full_file_name[40], file_name[20];
    printf("\t\tRead list from TEXT/BIN file located in './data' directory\n");
    printf("\tFile name :");
    scanf("%s", file_name);
    sprintf(full_file_name, "./data/%s", file_name);
    fd = fopen(full_file_name, (mode == TEXT) ? "rt" : "rb");
  } while(!fd);

  struct list * L = new_list();
  while(!feof(fd)) {
    void * data = (*read_data_ptrf)(fd, mode);
    if(!data) continue;
    insert_ordered(L, data, cmp_data_ptrf);
    // cons(L, data)
    // queue(L, data);
  }

  fclose(fd);
  return L;
}

/**
SjyGVAPqmgfCovvfZSYN
114yJ-seqZLChY88VibC
*/

#include <stdlib.h>
#include <stdio.h>

#include "form.h"
#include "list.h"
#include "db.h"

int main() {
  // form read from file function pointer
  // Warning: need to cast result function type (struct form *) into (void *)
  void * (* read_ptrf)(FILE *, enum mode_t) = (void * (*)()) &read_form;
  
  // form view function pointer
  void (* view_ptrf)(struct form *) = &view_form;

  // form write into file function pointer
  void (* write_ptrf)(struct form *, enum mode_t, FILE *)  = &write_form;

  // form deletion function pointer
  void (* del_ptrf)(struct form *) = &del_form;

  // forms comparison function pointer
  bool (* cmp_ptrf)(struct form *, struct form *) = &lt_form;
  
  // create form list from text/binary file wrt products' names acending order
  struct list * L = read_list(TEXT, read_ptrf, cmp_ptrf);

  // view form list
  view_list(L, view_ptrf);

  // write form list into text/binary file
  write_list(L, BINARY, write_ptrf);

  // delete form list
  del_list(L, del_ptrf);

  // create form list from text/binary file
  L = read_list(BINARY, read_ptrf, cmp_ptrf);

  // view form list
  view_list(L, view_ptrf);

  // delete form list
  del_list(L, del_ptrf);

  return EXIT_SUCCESS;
}

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "form.h"
#include "elmlist.h"
#include "list.h"

/*********************************
DÉCLARATIONS DES FONCTIONS PRIVÉES
*********************************/
void insert_after(struct list * L, void * data, struct elmlist * ptrelm);

/**********************************
DÉFINITIONS DES FONCTIONS PUBLIQUES
**********************************/
/** Create empty list  */
struct list * new_list() {
  struct list * L = (struct list *) calloc(1, sizeof(struct list));
  assert(L != NULL);
  return L;
}

/** Delete list its elements and the data they hold */
void del_list(struct list * L, void (*ptrf) ()) {
  assert(L != NULL);
  for(struct elmlist * iterator = L->head; iterator; ) {
    // note that there is no iteration step
    struct elmlist * E = iterator;

    // The iteration step needs to be defined here
    iterator = iterator->suc;
    del_elmlist(E, ptrf);
  }
  free(L);
}

/** Is list L empty ? */
bool is_empty(struct list * L) {
  assert(L != NULL);
  return L->numelm == 0;
}

/** Gimme the head of L*/
struct elmlist * get_head(struct list * L) {
  assert(L != NULL);
  return L->head;
}

/** Gimme the tail of L */
struct elmlist * get_tail(struct list * L) {
  assert(L != NULL);
  return L->tail;
}

/** Add a brand new list element holding data to the head of L */
void cons(struct list * L, void * data) {
  assert(L != NULL);
  struct elmlist * E = new_elmlist(data);
  E->suc = L->head;
  if( L->head ) {
    L->head->pred = E;
  } else {
    L->tail = E;
  }
  L->head = E;
  L->numelm += 1;
}

/** Add  a brand new list element holding data to the tail of L */
void queue(struct list * L, void * data) {
  assert(L != NULL);
  struct elmlist * E = new_elmlist(data);
  E->pred = L->tail;
  if ( L->tail ) {
    L->tail->suc = E;
  } else {
    L->head = E;
  }
  L->tail = E;
  L->numelm += 1;
}

/** Insert into L a new element holding data w.r.t. the order givent by cmp_ptrf */
void insert_ordered(struct list * L, void * data, bool (*cmp_ptrf) ()) {
  if( is_empty(L) || (*cmp_ptrf)(data, get_data(get_head(L))) ) {
    cons(L, data);
  } else if( (*cmp_ptrf)(get_data(get_tail(L)), data) ) {
    queue(L, data);
  } else {
    struct elmlist * iterator = L->head;
    bool found = false;
    while(iterator && !found) {
      found = (*cmp_ptrf)(data, get_data(iterator));
      if(!found) {
        iterator = iterator->suc;
      }
    }
    insert_after(L, data, iterator->pred);
  }
}

/** Well well well view list indeed */
void view_list(struct list * L, void (*ptrf)()) {
  printf("\t\t====================\n");
  printf("\t\t|| View data list ||\n");
  printf("\t\t====================\n");
  if(is_empty(L)) {
    printf("[ ] //empty list\n");
  }
  else {
    struct elmlist * iterator = L->head;
    while(iterator) {
      view_elmlist(iterator, ptrf);
      iterator = iterator->suc;
    }
  }
  printf("\t\t====================\n\n");
}

/*******************************************
Définitions des fonctions privées du TA list
*******************************************/

void insert_after(struct list * L, void * data, struct elmlist * place) {
  assert(L != NULL);
  if( is_empty(L) || !place) {
    cons(L,data);
  } else if( place == get_tail(L)) {
    queue(L, data);
  } else {
    struct elmlist * E = new_elmlist(data);
    E->pred = place;
    if(place == L->tail) {
      L->tail = E;
    } else {
      place->suc->pred = E;
    }
    E->suc = place->suc;
    place->suc = E;
    L->numelm += 1;
  }
}

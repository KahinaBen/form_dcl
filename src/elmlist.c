#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "form.h"
#include "elmlist.h"

/** Create a new `elmlist` around `data` */
struct elmlist * new_elmlist(void * data ) {
  struct elmlist * E = (struct elmlist *)calloc(1, sizeof(struct elmlist));
  assert(E != NULL);
  E->data = data;
  return E;
}

/* Delete elmlist and its data */
void del_elmlist(struct elmlist * E, void (*ptrf) ()) {
  assert(E != NULL);
  (*ptrf)(E->data);
  free(E);
}

/** Who's next ? */
struct elmlist * get_suc(struct elmlist * E) {
  assert(E != NULL);
  return E->suc;
}

/** Who's before ? */
struct elmlist * get_pred(struct elmlist * E) {
  assert(E != NULL);
  return E->pred;
}

/** What is the data ? */
void * get_data(struct elmlist * E) {
  assert(E != NULL);
  return E->data;
}

/** Change elmlist's successor for S */
void set_suc(struct elmlist * E, struct elmlist * S) {
  assert(E != NULL);
  E->suc = S;
}

/** Change elmlist's predecessor for P */
void set_pred(struct elmlist * E, struct elmlist * P) {
  assert(E != NULL);
  E->pred = P;
}

/** Change elmlist's data for data */
void set_data(struct elmlist * E, void * data) {
  assert(E != NULL);
  E->data = data;
}


void view_elmlist(struct elmlist * E, void (*ptrf)()) {
  (*ptrf)(E->data);
}

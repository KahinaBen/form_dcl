#ifndef _ELMLIST_
#define _ELMLIST_

#include <stdbool.h>
#include "form.h"

/**
Abstract type modeling a list element containing
- a form
- 2 pointers to its predecessor  and successor
*/
struct elmlist {
  void * data;
  struct elmlist * suc;
  struct elmlist * pred;
};

struct elmlist * new_elmlist(void * data );

void del_elmlist(struct elmlist * E, void (*ptrf) ());

struct elmlist * get_suc(struct elmlist * E);

struct elmlist * get_pred(struct elmlist * E);

void * get_data(struct elmlist * E);

void set_suc(struct elmlist * E, struct elmlist * S);

void set_pred(struct elmlist * E, struct elmlist * P);

void set_data(struct elmlist * E, void * data);

void view_elmlist(struct elmlist * E, void (*ptrf)());

#endif // _ELMLIST_

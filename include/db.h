#ifndef _DB_
#define _DB_

#include <stdio.h>
#include "list.h"

/** file mode : text or binary */
enum mode_t { TEXT, BINARY };

/** write a list into a text or binary file wrt the mode */
void write_list(struct list * L, enum mode_t mode, void (*ptrf)());

/** read a list from a text or binary file wrt the mode */
struct list * read_list(
  enum mode_t mode,
  void * (*read_data_ptrf)(),
  bool (*cmp_data_ptrf)()
);

#endif // _DB_
